import { Component, OnInit } from '@angular/core';
import { CartService } from '../../shared/cart.service';

@Component({
  selector: 'gog-navigation',
  template: `
    <nav class="nav-bar">
      <a href="/" class="nav-bar__branding">
        <img class="branding__image" src="assets/image/logo_classic.png">
      </a>
      <span class="nav-bar__cart-button" [ngClass]="{'nav-bar__cart-button--active': showBasket}" (click)="toggleBasket()">
        <img class="cart-button__icon" src="assets/image/ico_cart.svg">
        <span class="cart-button__item-count">{{cartCount}}</span>
      </span>
      <div class="nav-bar__cart" [ngClass]="{'nav-bar__cart--visible': showBasket}">
        <gog-cart></gog-cart>
      </div>
    </nav>
  `,
  styles: [`
    /* Host Component (gog-navigation) */
    :host {
      grid-column: 1 / -1;
      display: grid;
      grid-template-columns: 1fr 80% 1fr;
      grid-template-areas:
        ". navigation .";
      height: 56px;
      background: #ddd;
      background: linear-gradient(#eee, #ddd);
      box-shadow: 0 1px 3px rgba(0,0,0, 0.15), 0 0 15px rgba(0,0,0,0.15), 0 10px 100px 100px rgba(107,53,173,0.10);
      margin-bottom: 40px;
    }
    /* Nav Bar */
    .nav-bar {
      grid-area: navigation;
      display: grid;
      grid-template-columns: repeat(auto-fill, minmax(56px, 1fr));
      grid-template-rows: 56px auto;
      height: 56px;
      z-index: 1;
    }
    .nav-bar__branding {
      grid-column: 1 / span 1;
      align-self: center;
    }
    .branding__image {
      width: 39px;
    }
    /* Cart Button */
    .nav-bar__cart-button {
      grid-column: span 1 / -1;
      display: grid;
      grid-template-columns: auto auto;
      align-items: center;
      justify-content: center;
      grid-gap: 0 5px;
      cursor: pointer;
      border-left: 1px solid rgba(0,0,0,0.08);
      border-right: 1px solid rgba(0,0,0,0.08);
    }
    .nav-bar__cart-button--active {
      background: #f7f7f7;
    }
    /* Cart Button Items */
    .cart-button__icon {
      width: 16px;
    }
    .cart-button__item-count {
      color: #404040;
    }
    /* Cart */
    .nav-bar__cart {
      opacity: 0;
      grid-column: span 6 / -1;
      grid-row: 2;
    }
    .nav-bar__cart--visible {
      opacity: 1;
    }
  `]
})
export class NavigationComponent implements OnInit {
  showBasket: boolean;
  cartCount: Number = 0;

  constructor(private cartService: CartService) { }

  ngOnInit() {
    this.showBasket = false;
    this.cartService.cartItemCountSubject
      .subscribe(
        (cartCount) => this.cartCount = cartCount,
        (error) => console.log(error)
      );
  }

  toggleBasket() {
    this.showBasket = !this.showBasket;
    return;
  }

}
