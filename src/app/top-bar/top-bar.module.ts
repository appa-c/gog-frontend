import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './navigation/navigation.component';
import { CartComponent } from './cart/cart.component';
import { CartService } from '../shared/cart.service';
import { NameTruncatePipe } from '../shared/name-truncate.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [NavigationComponent],
  providers: [CartService, NameTruncatePipe],
  declarations: [NavigationComponent, CartComponent, NameTruncatePipe]
})
export class TopBarModule { }
