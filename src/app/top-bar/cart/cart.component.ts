import { Component, OnInit } from '@angular/core';
import { CartService } from '../../shared/cart.service';
import { Product } from '../../shared/Product';

@Component({
  selector: 'gog-cart',
  template: `
    <div class="cart">
      <div class="cart__summary">
        <span class="cart__count cart__summary-item">
          {{cartCount}} items in cart
        </span>
        <span class="cart__total cart__summary-item">
          $ {{cartValue}}
        </span>
        <button class="cart__clear-button cart__summary-item" (click)="clearCart()">
          clear cart
        </button>
      </div>
      <span *ngIf="cart.length > 0; else emptyCartTemplate">
        <div class="cart__items">
            <span class="cart__item" *ngFor="let product of cart">
              <img class="cart-item__image" src="assets/image/products/{{product.id}}.jpg">
              <h1 class="cart-item__name">{{product.name | nameTruncate: 25}}</h1>
              <span class="cart-item__price product__price">$ {{product.price}}</span>
              <small class="cart-item__remove" (click)="removeFromCart(product)">Remove</small>
            </span>
        </div>
      </span>
      <ng-template #emptyCartTemplate>
        <span class="cart__empty-cart">
          <p>Your cart is empty.</p>
        </span>
      </ng-template>
    </div>
  `,
  styles: [`
    /* Cart */
    .cart {
      display: grid;
      grid-template-rows: 60px auto;
      background: #f7f7f7;
      box-shadow: 0 3px 10px -1px rgba(0,0,0,0.15), 0 0 5px -5px rgba(0,0,0,0.10);
      border: 1px solid rgba(0,0,0,0.20);
      border-radius: 0 0 2px 2px;
    }
    /* Cart Summary */
    .cart__summary {
      grid-column: 1 / -1;
      display: flex;
      flex-wrap: wrap;
      height: 60px;
      text-transform: uppercase;
      padding: 0 5px;
      color: #1a1a1a;
      font-weight: 700;
    }
    .cart__summary-item {
      align-self: center;
      flex-grow: 1;
    }
    .cart__clear-button {
      box-shadow: 0 1px 3px rgba(0,0,0,0.30), 0 0 1px rgba(107,53,173,0.80);
      background: #793DC5;
      background: linear-gradient(#B559E1, #793DC5);
      color: #fff;
      height: 40px;
    }
    .cart__clear-button:active {
      background: linear-gradient(to top, #B559E1, #793DC5);
    }
    /* Cart Items */
    .cart__items {
      grid-column: 1 / -1;
    }
    /* Cart Item */
    .cart__item {
      display: grid;
      grid-template-columns: 100px 1fr auto;
      grid-template-rows: auto auto;
      grid-gap: 0 10px;
      align-items: center;
      padding: 5px;
      border-top: 1px solid rgba(0,0,0,0.08);
      height: 60px;
    }
    /* Cart Item Inner */
    .cart-item__image {
      grid-column: 1 / span 1;
      grid-row: 1 / span 2;
    }
    .cart-item__name {
      grid-column: span 1 / -2;
      grid-row: 1;
    }
    .cart-item__price {
      grid-column: span 1 / -1;
      grid-row: 1 / span 2;
    }
    .cart-item__remove {
      opacity: 0;
      text-decoration: underline;
      cursor: pointer;
    }
    .cart__item:hover .cart-item__remove {
      opacity: 1;
    }
    /* Empty Cart */
    .cart__empty-cart {
      text-align: center;
    }
  `]
})
export class CartComponent implements OnInit {
  cart: Product[] = [];
  cartCount: Number = 0;
  cartValue: Number = 0;

  constructor(private cartService: CartService) { }

  ngOnInit() {
    this.cartService.cartSubject
      .subscribe(
        (products) => this.cart = products,
        (error) => console.log(error)
      );
    this.cartService.cartItemCountSubject
      .subscribe(
        (cartCount) => this.cartCount = cartCount,
        (error) => console.log(error)
      );
    this.cartService.cartValueSubject
      .subscribe(
        (cartValue) => this.cartValue = cartValue,
        (error) => console.log(error)
      );
  }

  removeFromCart(product) {
    this.cartService.removeFromCart(product);
  }

  clearCart() {
    this.cartService.clearCart();
  }
}
