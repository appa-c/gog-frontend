import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../shared/Product';
import { CartService } from '../../shared/cart.service';

@Component({
  selector: 'gog-small-spot-item',
  template: `
    <div class="spot-card">
      <img class="spot-card__image" src="assets/image/products/{{product.id}}.jpg">
      <div class="spot-card__body" [ngClass]="{'muted': product.isOwned}">
        <h1 class="spot-card__header">{{product.name}}</h1>
        <div class="spot-card__details">
          <span class="spacer"></span>
          <span class="spot-card__discount" *ngIf="product.discountMulti < 1">-{{product.discountMulti * 100}}%</span>
          <button class="spot-card__price" (click)="addToCart()" *ngIf="!product.isOwned; else ownedProductTemplate">
            <span [ngSwitch]="isInCart()">
              <span *ngSwitchCase="true">
                in cart
              </span>
              <span *ngSwitchCase="false">
                $ {{product.price}}
              </span>
            </span>
          </button>
          <ng-template #ownedProductTemplate>
            <button class="spot-card__price">
              owned
            </button>
          </ng-template>
        </div>
      </div>
    </div>
  `,
  styles: [`
    /* Spot Card */
    .spot-card {
      display: grid;
      grid-template-rows: auto 1fr;
      background: #e1e1e1;
      box-shadow: 0 1px 5px rgba(0,0,0, 0.15);
      height: 100%;
    }
    /* Spot Card Body */
    .spot-card__body {
      display: flex;
      flex-direction: column;
      padding: 10px;
    }
    /* Spot Card Body Inner */
    .spot-card__header {
      flex-grow: 1;
      margin-bottom: 5px;
    }
    .spot-card__details {
      display: flex;
      align-items: stretch;
    }
    /* Spot Card Details Inner */
    .spacer {
      flex: 1;
    }
    .spot-card__discount {
      text-align: center;
      margin-right: 5px;
      color: #f2f2f2;
      background: #008f5d;
      padding: 3px;
      border-radius: 3px;
      width: 20%;
    }
    .spot-card__price {
      border: 1px solid rgba(0,0,0,0.25);
      background: transparent;
      padding: 3px;
      box-shadow: 0 1px 3px rgba(0,0,0,0.25);
      color: rgba(64,64,64,0.75);
      width: 40%;
      height: 30px;
    }
    .spot-card__price:hover {
      color: #f2f2f2;
      background: #9fbf00;
      background: linear-gradient(#9fbf00,#80ab00);
    }
    .spot-card__price:active {
      background: #80ab00;
      background: linear-gradient(#80ab00,#9fbf00);
    }
    .spot-card__price--owned {
      cursor: default;
    }
  `]
})
export class SmallSpotItemComponent implements OnInit {
  @Input() product: Product;

  constructor(private cartService: CartService) { }

  ngOnInit() {
  }

  addToCart() {
    this.cartService.addToCart(this.product);
  }

  isInCart() {
    return this.cartService.isInCart(this.product.id);
  }
}
