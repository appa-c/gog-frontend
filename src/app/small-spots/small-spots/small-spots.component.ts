import { Component, OnInit } from '@angular/core';
import { InventoryService } from '../../shared/inventory.service';
import { Product } from '../../shared/Product';

@Component({
  selector: 'gog-small-spots',
  template: `
    <div class="small-spots">
      <gog-small-spot-item *ngFor="let product of products" [product]="product"></gog-small-spot-item>
    </div>
  `,
  styles: [`
    /* Host Component (gog-small-spots) */
    :host {
      grid-column: 2 / 3;
    }
    /* Small Spots (Grid) */
    .small-spots {
      grid-column: 2 / 3;
      grid-area: small-spots;
      display: grid;
      grid-template-columns: repeat(auto-fit, minmax(196px, 1fr));
      grid-gap: 20px;
    }
  `]
})
export class SmallSpotsComponent implements OnInit {
  products: Product[];

  constructor(private inventory: InventoryService) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.products = this.inventory.getAllProducts();
  }

}
