import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmallSpotsComponent } from './small-spots/small-spots.component';
import { SmallSpotItemComponent } from './small-spot-item/small-spot-item.component';
import { InventoryService } from '../shared/inventory.service';
import { CartService } from '../shared/cart.service';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [SmallSpotsComponent],
  providers: [InventoryService, CartService],
  declarations: [SmallSpotsComponent, SmallSpotItemComponent]
})
export class SmallSpotsModule { }
