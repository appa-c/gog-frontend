export interface Product {
  id: string;
  name: string;
  rrp: number;
  price?: number;
  discountMulti: number;
  isFeatured: boolean;
  isOwned: boolean;
}
