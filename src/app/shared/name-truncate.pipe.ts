import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameTruncate'
})
export class NameTruncatePipe implements PipeTransform {

  transform(name, truncLength): String {
    if (name.length > truncLength) {
      return name.slice(0, truncLength - 3).trim().padEnd(truncLength, '.');
    } else {
      return name;
    }
  }

}
