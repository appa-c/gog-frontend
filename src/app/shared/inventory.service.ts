import { Injectable } from '@angular/core';
import { PRODUCTS } from './PRODUCTS';
import { Product } from './Product';

@Injectable()
export class InventoryService {

  constructor() { }

  getAllProducts(): Product[] {
    return PRODUCTS.filter((product) => {
      return product.isFeatured === false;
    }).map((product) => {
      product.price = product.rrp * product.discountMulti;
      return product;
    });
  }

  getFeaturedProduct(): Product {
    return PRODUCTS.filter((product) => {
      return product.isFeatured === true;
    })[0];
  }

  getProduct(id): Product {
    return PRODUCTS.filter((product) => {
      return product.id === id;
    })[0];
  }

}
