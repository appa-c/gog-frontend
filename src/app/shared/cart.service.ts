import { Injectable } from '@angular/core';
import { Product } from './Product';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class CartService {
  cart: Product[] = [];
  cartValue: Number = 0;
  cartSubject: Subject<Product[]>;
  cartItemCountSubject: Subject<Number>;
  cartValueSubject: Subject<Number>;

  constructor() {
    this.cartSubject = new Subject;
    this.cartItemCountSubject = new Subject;
    this.cartValueSubject = new Subject;
  }

  addToCart(product) {
    const isDupe = this.isInCart(product.id);
    if (isDupe) {
      return;
    } else {
      this.cart.push(product);
    }
    this.cartSubject.next(this.cart);
    this.updateCartItemCount();
    this.updateCartValue();
  }

  removeFromCart(product) {
    this.cart = this.cart.filter((item) => {
      return item.id !== product.id;
    });
    this.cartSubject.next(this.cart);
    this.updateCartItemCount();
    this.updateCartValue();
  }

  clearCart() {
    this.cart = [];
    this.cartSubject.next(this.cart);
    this.updateCartItemCount();
    this.updateCartValue();
  }

  isInCart(id) {
    return this.cart.reduce((arr, item) => {
      arr.push(item.id);
      return arr;
    }, []).includes(id);
  }

  private updateCartValue() {
    this.cartValue = this.cart.reduce((value, item) => {
      return value += item.price;
    }, 0);
    this.cartValueSubject.next(this.cartValue);
  }

  private updateCartItemCount() {
    this.cartItemCountSubject.next(this.cart.length);
  }

}
