import { Product } from './product';

export const PRODUCTS: Product[] = [
  {
    id: '1',
    name: 'The Witcher Adventure Game',
    rrp: 9.99,
    discountMulti: 1,
    isFeatured: true,
    isOwned: false,
  },
  {
    id: '2',
    name: 'Oddworld: Stranger\'s Wrath',
    rrp: 19.98,
    discountMulti: 0.5,
    isFeatured: false,
    isOwned: false,
  },
  {
    id: '3',
    name: 'Chaos On Deponia',
    rrp: 20.00,
    discountMulti: 1,
    isFeatured: false,
    isOwned: true,
  },
  {
    id: '4',
    name: 'The Settlers II: Gold Edition',
    rrp: 5.99,
    discountMulti: 1,
    isFeatured: false,
    isOwned: false,
  },
  {
    id: '5',
    name: 'Neverwinter Nights',
    rrp: 9.98,
    discountMulti: 0.5,
    isFeatured: false,
    isOwned: false,
  },
  {
    id: '6',
    name: 'Assasin\'s Creed: Director\'s Cut',
    rrp: 9.99,
    discountMulti: 1,
    isFeatured: false,
    isOwned: false,
  }
];
