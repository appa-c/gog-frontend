import { Component } from '@angular/core';

@Component({
  selector: 'gog-root',
  template: `
    <gog-navigation></gog-navigation>
    <gog-big-spot></gog-big-spot>
    <gog-small-spots></gog-small-spots>
  `,
  styles: [`
    :host {
      display: grid;
      grid-template-columns: 1fr 80% 1fr;
    }
  `]
})
export class AppComponent {
}
