import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { TopBarModule } from './top-bar/top-bar.module';
import { BigSpotModule } from './big-spot/big-spot.module';
import { SmallSpotsModule } from './small-spots/small-spots.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    TopBarModule,
    BigSpotModule,
    SmallSpotsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
