import { Component, OnInit } from '@angular/core';
import { InventoryService } from '../../shared/inventory.service';
import { Product } from '../../shared/Product';

@Component({
  selector: 'gog-big-spot',
  template: `
    <div class="big-spot">
      <h1 class="big-spot__title">game of the week</h1>
      <div class="big-spot__image-wrap">
        <img class="big-spot__image" src="assets/image/products/{{product.id}}.jpg">
        <button class="super-secret-button">
          Wow, a super secret button.
        </button>
      </div>
    </div>
  `,
  styles: [`
    /* Host Component (gog-big-spot) */
    :host {
      grid-column: 2 / 3;
      margin-bottom: 20px;
    }
    /* Big Spot */
    .big-spot {
      grid-area: big-spot;
    }
    /* Big Spot Inner */
    .big-spot__title {
      margin-bottom: 20px;
      font-weight: 700;
      color: #262626;
    }
    .big-spot__image-wrap {
      position: relative;
    }
    .big-spot__image {
      box-shadow: 0 1px 5px rgba(0,0,0,0.3);
    }
    /* Secret Button */
    .super-secret-button {
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      z-index: -1;
      color: #fff;
      padding: 5px;
      background: #008f5d;
      background: linear-gradient(#00c78b, #008f5d);
      box-shadow: 0 1px 3px rgba(0,0,0,0.30);
    }
    .super-secret-button:active {
      background: #00c78b;
      background: linear-gradient(#008f5d, #00c78b);
    }
  `]
})
export class BigSpotComponent implements OnInit {
  product: Product;

  constructor(private inventory: InventoryService) { }

  ngOnInit() {
    this.getFeaturedProduct();
  }

  getFeaturedProduct() {
    this.product = this.inventory.getFeaturedProduct();
  }

}
