import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BigSpotComponent } from './big-spot/big-spot.component';
import { InventoryService } from '../shared/inventory.service';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [BigSpotComponent],
  providers: [InventoryService],
  declarations: [BigSpotComponent]
})
export class BigSpotModule { }
