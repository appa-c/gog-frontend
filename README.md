# GOG Frontend Test

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Usage

Clone this repo to your local machine, and run:

```sh
ng serve --prod
```

To initialise a local server to serve the app in production mode at `localhost:4200`.

Alternatively, you can build the app and serve it using something like [http-server](https://www.npmjs.com/package/http-server).
